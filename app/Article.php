<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    
    protected $fillable = ['title', 'author', 'content'];

    public function hasImage()
    {
    	return $this->hasOne('App\Photo');
    }

    public function hasImages()
    {
    	return $this->hasMany('App\Photo');
    }
}
