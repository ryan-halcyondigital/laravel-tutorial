<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateArticleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:articles|min:5',
            'author' => 'required|min:3',
            'content'   => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Need to have title.',
            'title.unique'  => 'Sorry, title is already taken.',
            'title.min' => 'Please use 5 characters and up.',
            'author.required' => 'Who is the author of this article?'
        ];
    }
}
