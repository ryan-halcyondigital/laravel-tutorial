<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

	public function index()
	{
		return view('app.login');
	}

	public function attemptLogin(Request $request)
	{	
		$userCredentials = ['username' => $request->username, 'password' => $request->password];
		if (auth()->attempt($userCredentials)) {
			session()->flash('success', 'You are now logged in.');
			return redirect()->route('homepage');
		}
		else{
			session()->flash('failed', 'Username/Password did not match.');
			return redirect()->back();
		}
	}

	public function logout()
	{
		auth()->logout();
		return redirect()->route('homepage');
	}
}
