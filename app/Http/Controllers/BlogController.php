<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Article;
use App\Photo;

use App\Http\Requests\CreateArticleRequest;


class BlogController extends Controller
{	
	function __construct(Article $article, Photo $photo)
	{
		$this->article = $article;
        $this->photo = $photo;
	}

    public function index()
    {
    	$articles = $this->article->all();
    	return view('app.homepage', compact('articles'));
    }

    public function newArticle()
    {
    	return view('app.new');
    }

    public function createArticle(CreateArticleRequest $request)
    {	
    	$newArticle = ['title' => $request->title, 'author' => $request->author, 'content' => $request->content];
    	$article = $this->article->create($newArticle);
        if ($article) {
            $imageName = $request->file('image')->getClientOriginalName();
            if ($request->file('image')->move(public_path().'/uploads/', $imageName)) {
                $img = $this->photo->create(['article_id' => $article->id, 'image' => 'uploads/'.$imageName]);
            }
        }
   		return redirect()->route('homepage');
    }

    public function editArticle($id)
    {
    	$article = $this->article->find($id);
    	return view('app.edit', compact('article'));
    }

    public function updateArticle(CreateArticleRequest $request)
    {	
    	$editArticle = ['title' => $request->title, 'author' => $request->author, 'content' => $request->content];
    	$article = $this->article->find($request->id)->update($editArticle);
    	return redirect()->route('homepage');
    }

    public function deleteArticle($id)
    {
    	$article = $this->article->destroy($id);
    	return redirect()->back();
    }

}
