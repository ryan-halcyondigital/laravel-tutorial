<?php

Route::get('/', ['as' => 'homepage', 'uses' => 'BlogController@index']);
Route::get('login', ['as' => 'login', 'uses' => 'HomeController@index']);
Route::post('login', ['as' => 'attemptLogin', 'uses' => 'HomeController@attemptLogin']);

Route::group(['middleware' => 'auth'], function(){

	Route::get('new/article', ['as' => 'newArticle', 'uses' => 'BlogController@newArticle']);
	Route::post('new/article', ['as' => 'createArticle', 'uses' => 'BlogController@createArticle']);

	Route::get('article/{id}/edit', ['as' => 'editArticle', 'uses' => 'BlogController@editArticle']);
	Route::patch('article/update', ['as' => 'updateArticle', 'uses' => 'BlogController@updateArticle']);

	Route::get('article/{id}/delete', ['as' => 'deleteArticle', 'uses' => 'BlogController@deleteArticle']);
	Route::get('logout', ['as' => 'logout', 'uses' => 'HomeController@logout']);
});