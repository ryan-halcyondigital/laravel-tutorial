<?php

use Illuminate\Database\Seeder;
use App\Article;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Article::create(['title' => 'First Article', 'author' => 'Ryan', 'content' => 'This is my first article.']);
        Article::create(['title' => 'Second Article', 'author' => 'Ryan', 'content' => 'This is my second article.']);
    }
}
