@extends('app.layouts.master')

@section('content')

<h1>New Article</h1>

@include('app.partials.error')
<br />
{!! Form::open(['files' => true]) !!}
	@include('app.partials.form')
	<label>Photo</label><br />
	{!! Form::file('image') !!}
	<br /><br /> 
	{!! Form::submit('Create Article') !!}

{!! Form::close() !!}
<hr />
<a href="{{ route('homepage') }}"><< Homepage</a>


@stop