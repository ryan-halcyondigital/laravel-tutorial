@extends('app.layouts.master')

@section('content')
<h1>Login</h1>

@if(session()->has('failed'))
	<li>{{ session()->get('failed')}}</li>
@endif
{!! Form::open() !!}
	<label>Username</label><br />
	{!! Form::text('username', null) !!}
	<br /><br />

	<label>Password</label><br />
	{!! Form::password('password') !!}
	<br /><br />

	{!! Form::submit('Login') !!} | <a href="{{ route('homepage') }}">Homepage</a>
{!! Form::close() !!} 

@stop