@extends('app.layouts.master')

@section('content')

<h1>Ryan's Blog</h1>
@foreach($articles as $article)
	<p>{{ $article->title }}</p>
	@foreach($article->hasImages as $img)
		<img src="{{ asset($img->image) }}" class="img-responsive">
	@endforeach
	
	<article>
		<i>{{ $article->content }}</i>
	</article>
	<p>By:<em><b> {{ $article->author }}</b></em></p>
	<p><a href="{{ route('editArticle', $article->id) }}">Edit</a> | <a href="{{ route('deleteArticle', $article->id) }}">Delete</a></p>
	<hr />
@endforeach

@if(auth()->user())
<a href="{{ route('newArticle') }}">+ New Article |</a>
@endif 

@if(auth()->user())
	<a href="{{ route('logout') }}">Logout</a>
@else
	 <a href="{{ route('login') }}">Login</a>
@endif


@stop