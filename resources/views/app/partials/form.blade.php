{!! Form::text('title', null, ['placeholder' => 'Enter title']) !!}
<br /><br />
<label>Author</label><br />
{!! Form::text('author', null, ['placeholder' => 'Who is the author?']) !!}

<br /><br />
<label>Content</label><br />
{!! Form::textarea('content', null) !!}

<br /><br />