@extends('app.layouts.master')

@section('content')

@include('app.partials.error')

<h1>Edit Article</h1>
{!! Form::model($article, ['method' => 'PATCH', 'url' => route('updateArticle')]) !!}
	{!! Form::hidden('id')!!}
	@include('app.partials.form')

	{!! Form::submit('Update Article')!!}
{!! Form::close() !!}

<hr />

<a href="{{ route('homepage') }}"><< Homepage</a>

@stop